import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: georgiy
 * Date: 30.07.18
 * Time: 14:35
 * To change this template use File | Settings | File Templates.
 */
public class BaseTestLTD {
    WebDriver driver = initChromeDriver();
    @Test
    public void firstTest(){
        driver.get("https://mail.google.com/mail/#inbox" );
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        WebElement field = driver.findElement(By.id("identifierId"));
        field.sendKeys( "georgiyserdyuk88@gmail.com"  );
        WebElement button1 = driver.findElement(By.cssSelector("#identifierNext > content > span"));
        button1.click();
        WebElement field1 = driver.findElement(By.name("password"));
        field1.sendKeys( "Dollars888"  );
        WebElement button2 = driver.findElement(By.cssSelector("#passwordNext > content > span"));
        button2.click();
        WebElement button3 = driver.findElement(By.xpath("//*[@id=\":ap\"]/div/div"));
        button3.click();
        WebElement field2 = driver.findElement(By.id(":fs"));
        field2.sendKeys("georgiyserdyuk88@gmail.com");
        WebElement field3 = driver.findElement(By.id(":fh"));
        field3.sendKeys("TEST");
        WebElement button4 = driver.findElement(By.id(":f7"));
        button4.click();
        WebElement button5 = driver.findElement(By.className("J-Ke n0"));
        button5.click();
        WebElement letter = driver.findElement(By.id(":3d"));
        String letterText = letter.getText();
        assertEquals("TEST",letterText);
        System.out.println("Текст в письме совпадает");
        driver.quit();

    }
    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        return new ChromeDriver();
    }
}